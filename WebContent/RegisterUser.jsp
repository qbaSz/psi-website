<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Register a user</title>
<link href="Resources/bootstrap.css" rel="stylesheet">
</head>
<body>
<div class="container">
<div class="page-header">
	    <h1>Register a user</h1>
</div>
	<form  action="Register" method="post" class="form-horizontal">
		<div class="form-group">
			<label for="firstName" class="col-sm-2 control-label">First Name</label>
			<div class="col-sm-4">
				<input id="firstName" class="form-control" name="first_name">
			</div>
		</div>
		<div class="form-group">
			<label for="lastName" class="col-sm-2 control-label">Last name</label>
			<div class="col-sm-4">
				<input id="lastName" class="form-control" name="last_name">
			</div>
		</div>
		<div class="form-group">
			<label for="phoneNumber" class="col-sm-2 control-label">Phone number</label>
			<div class="col-sm-4">
				<input id="phoneNumber" class="form-control" name="phone_number">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2"><p class="error" style="color: red"> 
			<%
			 	if (request.getAttribute("error") != null) {
			 		out.println(request.getAttribute("error"));
			 	}
			 %>
			</p></div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-success">Apply</button>
				<button type="reset" class="btn btn-danger">Clear</button>
			</div>
		</div>
	</form>
	<a href="Index.jsp">Back to main page</a>
</div>
</body>
</html>