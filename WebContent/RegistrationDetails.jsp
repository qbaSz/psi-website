<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Main page</title>
</head>
<body>
	<h3>Registration details</h3>
	<table>
		<tr>
			<td>Name:</td>
			<td><%=request.getAttribute("name")%></td>
		</tr>
		<tr>
			<td>Surname:</td>
			<td><%=request.getAttribute("surname")%></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><%=request.getAttribute("email")%></td>
		</tr>
	</table>
	<a href="Index.jsp">Back to main page</a>
</body>
</html>