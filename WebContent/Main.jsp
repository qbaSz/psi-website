<%@taglib uri="http://java.sun.com/jstl/core" prefix="c1"%><%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Your logs</title>
<link href="Resources/bootstrap.css" rel="stylesheet">
</head>
<body>
	<%
		String username = request.getParameter("user");
		session.setAttribute("username", username);
	%>
	<sql:setDataSource var="db1"
		dataSource="jdbc:oracle:thin:@127.0.0.1:1521:xe,
		oracle.jdbc.OracleDriver,system,admin" />
		
	<sql:query var="query1" dataSource="${db1}">
		SELECT PASSWORD, LOG_TYPE, TO_CHAR(LOG_TIME, 'DD-MON-YYYY HH24:MI') AS LOG_TIME FROM LOGS WHERE USERNAME like '<%=session.getAttribute("username")%>' AND LOG_ATTEMPT = 'F'
	</sql:query>
	<sql:query var="query2" dataSource="${db1}">
		SELECT PASSWORD, LOG_TYPE, TO_CHAR(LOG_TIME, 'DD-MON-YYYY HH24:MI') AS LOG_TIME FROM LOGS WHERE USERNAME like '<%=session.getAttribute("username")%>' AND LOG_ATTEMPT = 'S'
	</sql:query>
	<sql:query var="query3" dataSource="${db1}">
		SELECT FIRST_NAME, LAST_NAME FROM PRACOWNICY WHERE PHONE_NUMBER like '<%=session.getAttribute("username")%>'
	</sql:query>
	
	<div class="container">
		<div class="page-header">
			<h1>User logs list</h1>
		</div>
		<div class="row">
			<div class="col-sm-5">
				 	<h4>Hello: <c:forEach var="row" items="${query3.rows}"><c:out value="${row.first_name}"/> <c:out value="${row.last_name}"/></c:forEach> </h4>
			</div>
			<div class="col-sm-1 col-sm-offset-6">
				<form action="LogOutController" method="post">
					<input type="hidden" name="username" value="${username}"/>
					<button type="submit" class="btn btn-warning">Log out</button>
				</form>
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-sm-5" style="overflow: auto; height: 500px">
				<p style="color: red">Unsuccessful attempts</p>
				<table class="table">
					<tr>
						<td>PASSWORD</td>
						<td>LOG TIME</td>
						<td>LOG TYPE</td>
					</tr>
			
					<c:forEach var="row" items="${query1.rows}">
						<tr>
							<td><c:out value="${row.password}" /></td>
							<td><c:out value="${row.log_time}" /></td>
							<td><c:if test="${row.log_type eq 'I'}">
									Login In
								</c:if> <c:if test="${row.log_type eq 'O'}">
									Login Out
								</c:if></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="col-sm-5 col-sm-offset-2" style="overflow: auto; height: 500px">
				<p style="color: green">Successful attempts</p>
				<table class="table">
					<tr>
						<td>PASSWORD</td>
						<td>LOG TIME</td>
						<td>LOG TYPE</td>
					</tr>
			
					<c:forEach var="row" items="${query2.rows}">
						<tr>
							<td><c:out value="${row.password}" /></td>
							<td><c:out value="${row.log_time}" /></td>
							<td><c:if test="${row.log_type eq 'I'}">
									Login In
								</c:if> <c:if test="${row.log_type eq 'O'}">
									Login Out
								</c:if></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</div>
</body>
</html>