<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Main page</title>
<link href="Resources/bootstrap.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="page-header"><h1>Check your logs </h1></div>
		<form action="LogInController" method="post">
			<div class="col-lg-4">
			    <h4>Log in</h4>
				<div class="form-group">
					<label for="userInput">User name</label>
					<input id="userInput" class="form-control" name="user" placeholder="username">
				</div>
				<div class="form-group">
					<label for="passwordInput">Password</label>
					<input id="passwordInput" class="form-control" name="pass" placeholder="password" type="password">
				</div>
				<p class="error" style="color: red"> 
				<%
				 	if (request.getAttribute("error") != null) {
				 		out.println(request.getAttribute("error"));
				 	}
				 %>
				</p>
				<button type="submit" class="btn btn-primary">Log in</button>
				<a class="btn btn-default" href="RegisterUser.jsp">Register user</a>
			</div>
		</form>
		
	</div>

</body>
</html>