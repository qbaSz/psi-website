/***************************************************/
/**************AT START*****************************/
/***************************************************/

CREATE TABLE PRACOWNICY
  AS (SELECT EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER, HIRE_DATE FROM HR.EMPLOYEES);
  
  
 CREATE TABLE "SYSTEM"."LOGS" 
   (	"USERNAME" VARCHAR2(20 BYTE), 
	"PASSWORD" VARCHAR2(30 BYTE), 
	"LOG_TIME" TIMESTAMP (6), 
	"LOG_ATTEMPT" CHAR(1 BYTE), 
	"LOG_TYPE" CHAR(1 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
  

/***************************************************/
/**************LOG_PROC*****************************/
/***************************************************/

create or replace PROCEDURE LOG_PROC 
(
  U_NAME IN VARCHAR2 
, PASSWORD IN VARCHAR2 
, LOG_TYPE IN CHAR
, SUCCESS OUT VARCHAR2
) AS 
    CNT NUMBER;
    EXIST NUMBER;
BEGIN
    SUCCESS := 'F';
    IF LOG_TYPE = 'O'
    THEN
         INSERT INTO LOGS VALUES(U_NAME, PASSWORD, CURRENT_TIMESTAMP, 'S', LOG_TYPE);
         SUCCESS := 'S';
    ELSE
        SELECT COUNT(USERNAME) INTO CNT FROM LOGS WHERE USERNAME like U_NAME AND LOG_TIME > (CURRENT_TIMESTAMP - INTERVAL '2' MINUTE) AND LOG_ATTEMPT = 'F';
        IF CNT > 5 THEN
            INSERT INTO LOGS VALUES(U_NAME, PASSWORD, CURRENT_TIMESTAMP, 'F', LOG_TYPE);
            SUCCESS := 'B';
        ELSE
            SELECT COUNT(PHONE_NUMBER) INTO EXIST FROM PRACOWNICY WHERE PHONE_NUMBER like U_NAME AND LAST_NAME like PASSWORD;
            IF EXIST = 1 THEN
                INSERT INTO LOGS VALUES(U_NAME, PASSWORD, CURRENT_TIMESTAMP, 'S', LOG_TYPE);
                SUCCESS := 'S';
            ELSE 
                INSERT INTO LOGS VALUES(U_NAME, PASSWORD, CURRENT_TIMESTAMP, 'F', LOG_TYPE);
            END IF;
        END IF;
    END IF;
END LOG_PROC;

/***************************************************/
/**************INSERT_PROC**************************/
/***************************************************/

create or replace PROCEDURE INSERT_PROC 
(
  I_FIRST_NAME IN VARCHAR2 
, I_LAST_NAME IN VARCHAR2 
, I_PHONE_NUMBER IN VARCHAR2
, SUCCESS OUT VARCHAR2
) AS 
    NUM_ID NUMBER;
    DATE_HIRE DATE;
    I_EMAIL VARCHAR2(25);
BEGIN

    SELECT max(EMPLOYEE_ID)+1 INTO NUM_ID FROM PRACOWNICY;
    SELECT CURRENT_DATE INTO DATE_HIRE FROM DUAL;
    I_EMAIL := CONCAT(SUBSTR(I_FIRST_NAME, 0, 1), I_LAST_NAME);
        SUCCESS := 'F';
    Insert into PRACOWNICY
    values (NUM_ID,I_FIRST_NAME,I_LAST_NAME,I_EMAIL,I_PHONE_NUMBER,TO_CHAR(DATE_HIRE,'RR/MM/DD'));
        SUCCESS := 'S';
END INSERT_PROC;

/***************************************************/
/**************TEST THINGS**************************/
/***************************************************/


select * from logs;

select * from pracownicy;

SELECT TO_CHAR(LOG_TIME, 'DD-MON-YYYY HH24:MI') FROM LOGS WHERE USERNAME like '515.123.4567' AND LOG_ATTEMPT = 'F';

SELECT COUNT(USERNAME) FROM LOGS WHERE LOG_TIME > (CURRENT_TIMESTAMP - INTERVAL '2' MINUTE);
dbms_output.put_line(current_timestamp);
EXECUTE LOG_PROC('515.123.4567', 'King', 'I');

DECLARE
A VARCHAR2(1);
BEGIN
LOG_PROC('515.123.4567', 'King', 'I', A);
END;