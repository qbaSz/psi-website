package controllers;

import java.io.IOException;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LogInController
 */
@WebServlet("/LogInController")
public class LogInController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogInController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = request.getParameter("user");
		String pass = request.getParameter("pass");
		String type = "I";

		String JDBC_DRIVER = "oracle.jdbc.OracleDriver";  
	    String DB_URL="jdbc:oracle:thin:@127.0.0.1:1521:xe";

	      //  Database credentials
	    String USER = "system";
	    String PASS = "admin";
		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.setAutoCommit(true);
			// Execute SQL query
			String sql;
			sql = "{call LOG_PROC(?, ?, ?, ?)}";
			CallableStatement stmt = conn.prepareCall(sql);
			stmt.setString(1, user);
			stmt.setString(2, pass);
			stmt.setString(3, type);
			stmt.registerOutParameter(4, Types.VARCHAR);
			stmt.executeUpdate();
			
			String output = stmt.getString(4);
			System.out.println(output);
			ServletContext context = getServletContext();
			RequestDispatcher dispatcher = null;
			System.out.println(output.charAt(0));
			if (output.charAt(0) == 'F') {
				request.setAttribute("error", "Wrong username or password");
				dispatcher = context.getRequestDispatcher("/Index.jsp");
			} else if (output.charAt(0) == 'B') {
				request.setAttribute("error", "You have been blocked for next 2 minutes");
				dispatcher = context.getRequestDispatcher("/Index.jsp");
			} else {
				request.setAttribute("user", user);
				dispatcher = context.getRequestDispatcher("/Main.jsp");
			}
			dispatcher.forward(request, response);
	        stmt.close();
	        conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		doGet(request, response);
	}

}
