package controllers;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterController
 */
@WebServlet({ "/RegisterController", "/Register" })
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String first_name = request.getParameter("first_name");
		String last_name = request.getParameter("last_name");
		String phone_number = request.getParameter("phone_number");

		String JDBC_DRIVER = "oracle.jdbc.OracleDriver";  
	    String DB_URL="jdbc:oracle:thin:@127.0.0.1:1521:xe";

	      //  Database credentials
	    String USER = "system";
	    String PASS = "admin";
	    try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.setAutoCommit(true);
			// Execute SQL query
			String sql;
			sql = "{call INSERT_PROC(?, ?, ?, ?)}";
			CallableStatement stmt = conn.prepareCall(sql);
			stmt.setString(1, first_name);
			stmt.setString(2, last_name);
			stmt.setString(3, phone_number);
			stmt.registerOutParameter(4, Types.VARCHAR);
			stmt.executeUpdate();
			
			String output = stmt.getString(4);
			System.out.println(output);
			ServletContext context = getServletContext();
			RequestDispatcher dispatcher = null;
			
			System.out.println(output.charAt(0));
			if (output.charAt(0) == 'F') {
				request.setAttribute("error", "Error in registration");
				dispatcher = context.getRequestDispatcher("/Index.jsp");
			} else {
				request.setAttribute("error", "Success! Now you can login!");
				dispatcher = context.getRequestDispatcher("/Index.jsp");
			}
			dispatcher.forward(request, response);
	        stmt.close();
	        conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
