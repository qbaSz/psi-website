package helpers;

import javax.servlet.http.HttpServletRequest;

public class Helper {
	public static String getAction(HttpServletRequest request) {
	    String act[] = request.getRequestURL().toString().split("/");
	    return act[act.length-1];
	}
}
